const cardContainer = document.querySelector('.cardContainer')


const cardInfo = [
    {
        name : 'Naruto',
        img : 'assets/img/pfp1.jpg',
        positon : 'Web Developer'
    },
    {
        name : 'Sasuke',
        img : 'assets/img/pfp2.jpg',
        positon : 'Mobile Developer'
    },
    {
        name : 'Kotarou',
        img : 'assets/img/pfp3.jpg',
        positon : 'Senior Developer'
    },    {
        name : 'Naruto',
        img : 'assets/img/pfp1.jpg',
        positon : 'Web Developer'
    },
    {
        name : 'Sasuke',
        img : 'assets/img/pfp2.jpg',
        positon : 'Mobile Developer'
    },
    {
        name : 'Kotarou',
        img : 'assets/img/pfp3.jpg',
        positon : 'Senior Developer'
    },
]


let content ='';
cardInfo.forEach(function(card){
    let html = `
    <div class="col-lg-4 pt-5">
                        <div class="card" >
                            <img class="img-fluid" src="${card.img}" class="card-img-top" alt="...">
                            <div class="card-body">
                              <div class="row">
                                <div class="col-md-9">
                                    <b>${card.name}</b>
                                    <p class="p3">${card.positon}</p>
                                  </div>
                                    <div class="col-md-3 text-center">
                                        <a href=""><i class="fa-brands fa-twitter iconcolor"></i></a>
                                        <a href=""><i class="fa-brands fa-linkedin iconcolor"></i></a>
                                    </div>
                              </div>
                            </div>
                        </div>
                    </div>
  `
  content += html;
  cardContainer.innerHTML = content;
}
)


